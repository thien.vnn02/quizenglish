import React from 'react';
import {Provider} from 'react-redux';
import {persistor, store} from './Store';
import {PersistGate} from 'redux-persist/integration/react';
import AppContainer from './Navigations';
import { SafeAreaProvider } from "react-native-safe-area-context/src/SafeAreaContext";

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SafeAreaProvider>
          <AppContainer />
        </SafeAreaProvider>
      </PersistGate>
    </Provider>
  );
};
export default App;
