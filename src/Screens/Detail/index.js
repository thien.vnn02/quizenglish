import React, {useEffect, useMemo, useRef, useState} from 'react';
import {
  ImageBackground, Platform,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";

import {useNavigation} from '@react-navigation/native';
import {BackArrow, SClock} from '@svg';
import Information from './components/Information';
import Swiper from 'react-native-swiper';
import Test from './components/Test';
import CountDownTimer from 'react-native-countdown-timer-hooks';
import {useDispatch, useSelector} from 'react-redux';
import {MyText} from '@components';
import {computeResult} from '../../Store/Result';

const DetailScreen = () => {
  const {goBack, navigate} = useNavigation();
  const [isStart, setStart] = useState(false);
  const [index, setIndex] = useState(0);
  const refTimer = useRef();
  const [timerEnd, setTimerEnd] = useState(false);
  const question = useSelector(state => state.test);
  const answer = useSelector(state => state.result.answer);
  const {testSelect, time, title} = question.testSelect;
  const type = useSelector(state => state.type.type);
  const dispatch = useDispatch();

  const timerCallbackFunc = timerFlag => {
    // Setting timer flag to finished
    setTimerEnd(timerFlag);
    navigate('Result', {restart});
  };

  const restart = () => {
    setStart(true);
    setIndex(1);
  };

  const submit = question => {
    setStart(false);
    setIndex(0);
    dispatch(computeResult({question: question}));
    navigate('Result', {restart});
  };

  const Header = () => {
    return (
      <View style={styles.header}>
        <ImageBackground
          source={require('../../../assets/backgroundDetail.png')}
          resizeMode={'cover'}
          style={{width: '100%', height: '100%'}}>
          <SafeAreaView>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 10,
                alignItems: 'center',
              }}>
              <View style={{flexDirection: 'row', paddingLeft: 24}}>
                <TouchableOpacity onPress={goBack}>
                  <BackArrow />
                </TouchableOpacity>
                <MyText style={styles.title}>{title}</MyText>
              </View>
              {isStart && type !== 2 && (
                <View style={styles.clock}>
                  <SClock />
                  <CountDownTimer
                    ref={refTimer}
                    timestamp={time * 60}
                    timerCallback={timerCallbackFunc}
                    containerStyle={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderRadius: 35,
                    }}
                    textStyle={{
                      fontSize: 12,
                      color: '#FFFFFF',
                      fontWeight: Platform.OS === 'ios' ? '500' : 'bold',
                      letterSpacing: 0.25,
                      marginLeft: 5,
                    }}
                  />
                </View>
              )}
            </View>
          </SafeAreaView>
        </ImageBackground>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.content}>
        <Swiper
          loop={false}
          showsPagination={false}
          index={index}
          scrollEnabled={false}>
          <Information
            start={() => {
              setStart(true);
              setIndex(1);
            }}
            test={question.testSelect}
          />
          <Test
            submit={submit}
            question={type === 2 ? answer : question.questions}
          />
        </Swiper>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    width: '100%',
    height: '50%',
  },
  content: {
    position: 'absolute',
    width: '100%',
    height: '88%',
    bottom: 0,
    backgroundColor: 'white',
    borderTopEndRadius: 32,
    borderTopStartRadius: 32,
    paddingTop: 26,
    paddingLeft: 24,
  },
  clock: {
    flexDirection: 'row',
    backgroundColor: '#CA0E0E',
    marginLeft: 21,
    alignItems: 'center',
    paddingVertical: 8,
    paddingHorizontal: 13,
    borderRadius: 12,
    marginRight: 20,
  },
  title: {
    marginLeft: 16,
    fontWeight: Platform.OS === 'ios' ? '500': 'bold',
  },
});

export default DetailScreen;
