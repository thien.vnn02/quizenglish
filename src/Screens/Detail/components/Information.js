import React from 'react';
import { Platform, StyleSheet, TouchableOpacity, View } from "react-native";
import {MyText} from '@components';
import {MDocument, MClock} from '@svg';

const Information = ({start, test}) => {
  const {totalQuestion, time} = test
  const Info = ({title, description, icon, colorIcon}) => {
    return (
      <View style={styles.containerInfo}>
        <View style={[styles.containerIcon, {backgroundColor: colorIcon}]}>
          {icon}
        </View>
        <View>
          <MyText style={{fontSize: 16}}>{title}</MyText>
          <MyText style={{fontSize: 14, color: '#999999'}}>
            {description}
          </MyText>
        </View>
      </View>
    );
  };

  const DotText = ({text}) => {
    return (
      <View style={{flexDirection: 'row'}}>
        <MyText>●</MyText>
        <MyText style={{marginLeft: 16, marginRight: 25}}>{text}</MyText>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <MyText style={styles.title}>Brief explanation about this quiz</MyText>
      <Info
        icon={<MDocument />}
        colorIcon="#1BC0E4"
        title={`${totalQuestion} Questions`}
        description="10 point for a correct answer"
      />
      <Info
        icon={<MClock />}
        colorIcon="#CA0E0E"
        title={`${time} min`}
        description="Total duration of the quiz"
      />
      <MyText style={[styles.title, {marginTop: 28}]}>
        Please read the text below carefully so you can understand it
      </MyText>
      <DotText text="A word or phrase is missing in each of the sentences below." />
      <DotText text="Four answer choices are given below each sentence." />
      <DotText text="Select the best answer to complete the sentence." />
      <DotText text="Then mark the letter (A), (B), (C), or (D) on your answer sheet." />
      <DotText text="Click the button Submit Quiz if you want to complete your test." />
      <TouchableOpacity style={styles.buttonStart} onPress={start}>
        <MyText style={styles.textStart}>Start Quiz</MyText>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  title: {
    fontWeight: 'bold',
  },
  containerInfo: {
    marginTop: 19,
    flexDirection: 'row',
    alignItems: 'center',
  },
  containerIcon: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    marginRight: 16,
  },
  buttonStart: {
    alignSelf: 'center',
    width: '90%',
    backgroundColor: '#3A8C38',
    borderRadius: 16,
    position: 'absolute',
    bottom: 50,
    left: 10,
  },
  textStart: {
    alignSelf: 'center',
    marginTop: 16,
    marginBottom: 15,
    fontWeight: Platform.OS === 'ios' ? '700' : 'bold',
    color: 'white',
    fontSize: 16,
  },
});
export default Information;
