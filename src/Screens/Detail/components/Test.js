import React, {useEffect, useRef, useState} from 'react';
import { FlatList, Platform, StyleSheet, TouchableOpacity, View } from "react-native";
import {MyText} from '@components';
import {Check, MArrowLeft, MArrowRight, Close} from '@svg';
import {useSelector} from 'react-redux';

const Test = ({submit, question}) => {
  const [index, setIndex] = useState(0);
  const [layout, setLayout] = useState({
    width: 0,
    height: 0,
  });
  const refItem = useRef();
  const type = useSelector(state => state.type.type);
  const [tmpQuestion, setQuestions] = useState(question);

  useEffect(() => {
    if (type === 1 || type === 2) {
      setIndex(0);
      refItem.current.scrollToIndex({
        index: '' + index,
      });
      setQuestions(question);
    }
  }, [type]);

  const Question = ({item, index}) => {
    const [tmpItem, setItem] = useState(item);
    let newQuestions = [...tmpQuestion];

    const pickAnswer = i => {
      let newItem = JSON.parse(JSON.stringify(tmpItem));
      switch (i) {
        case 0:
          newItem.B.isSelected = false;
          newItem.C.isSelected = false;
          newItem.D.isSelected = false;
          newItem.A.isSelected = true;
          break;
        case 1:
          newItem.B.isSelected = true;
          newItem.C.isSelected = false;
          newItem.D.isSelected = false;
          newItem.A.isSelected = false;
          break;
        case 2:
          newItem.B.isSelected = false;
          newItem.C.isSelected = true;
          newItem.D.isSelected = false;
          newItem.A.isSelected = false;
          break;
        case 3:
          newItem.B.isSelected = false;
          newItem.C.isSelected = false;
          newItem.D.isSelected = true;
          newItem.A.isSelected = false;
          break;
        default:
          break;
      }
      newQuestions[index] = newItem;
      setQuestions(newQuestions);
      setItem(newItem);
    };

    const isNotAnswer = () => {
      return (
        !tmpItem.A.isSelected &&
        !tmpItem.B.isSelected &&
        !tmpItem.C.isSelected &&
        !tmpItem.D.isSelected
      );
    };

    return (
      <View style={{width: layout.width}}>
        <MyText style={styles.question}>{tmpItem.Answer}</MyText>
        <Answers
          a={tmpItem.A}
          index={0}
          pickAnswer={pickAnswer}
          isNotAnswer={type === 2 && isNotAnswer()}
        />
        <Answers
          a={tmpItem.B}
          index={1}
          pickAnswer={pickAnswer}
          isNotAnswer={type === 2 && isNotAnswer()}
        />
        <Answers
          a={tmpItem.C}
          index={2}
          pickAnswer={pickAnswer}
          isNotAnswer={type === 2 && isNotAnswer()}
        />
        <Answers
          a={tmpItem.D}
          index={3}
          pickAnswer={pickAnswer}
          isNotAnswer={type === 2 && isNotAnswer()}
        />
      </View>
    );
  };

  const checkScreen = (isSelected, isCorrect, isNotAnswer) => {
    let background, iconBackground;
    if (isSelected && type === 2) {
      if (isCorrect) {
        background = '#3A8C38';
        iconBackground = '#FFFFFF';
      } else {
        background = 'rgb(242,200,200)';
        iconBackground = '#CA0E0E';
      }
    } else if (type === 2 && isCorrect && !isNotAnswer) {
      background = '#3A8C38';
      iconBackground = '#FFFFFF';
    } else if (isSelected) {
      background = 'rgb(211, 229, 209)';
      iconBackground = '#3A8C38';
    } else if (isCorrect && isNotAnswer) {
      background = 'rgb(242,200,200)';
      iconBackground = '#CA0E0E';
    } else {
      background = '#FFFFFF';
      iconBackground = '#FFFFFF';
    }
    return {
      background: background,
      iconBackground: iconBackground,
    };
  };

  const Answers = React.memo(({a, index, pickAnswer, isNotAnswer}) => {
    const {text, isSelected, isCorrect} = a;
    return (
      <TouchableOpacity
        style={[
          styles.answer,
          {
            backgroundColor: checkScreen(isSelected, isCorrect, isNotAnswer)
              .background,
            borderWidth: !isSelected ? 2 : 0,
            borderColor: !isSelected && '#DDDDDD',
          },
        ]}
        onPress={() => {
          pickAnswer(index);
        }}
        activeOpacity={1}
        disabled={type === 2}>
        <View
          style={[
            styles.checkBox,
            {
              backgroundColor: checkScreen(isSelected, isCorrect, isNotAnswer)
                .iconBackground,
              borderWidth: !isSelected ? 2 : 0,
              borderColor: !isSelected && '#DDDDDD',
            },
          ]}>
          {isSelected && type !== 2 && (
            <Check color={isSelected && type !== 2 && '#FFFFFF'} />
          )}
          {isCorrect && type == 2 && !isNotAnswer && (
            <Check color={'#3A8C38'} />
          )}
          {isSelected && !isCorrect && type == 2 && !isNotAnswer && <Close />}
          {isNotAnswer && <Close />}
        </View>
        <MyText style={styles.option}>{text}</MyText>
      </TouchableOpacity>
    );
  });

  return (
    <View style={styles.container}>
      <MyText style={styles.questionNum}>
        QUESTION {index + 1} OF {question.length}
      </MyText>

      <View
        style={{flex: 1}}
        onLayout={event => setLayout(event.nativeEvent.layout)}>
        <FlatList
          data={tmpQuestion}
          renderItem={({item, index}) => {
            return <Question item={item} index={index} />;
          }}
          keyExtractor={item => item.STT.toString()}
          horizontal
          showsHorizontalScrollIndicator={false}
          scrollEnabled={false}
          initialScrollIndex={index}
          ref={refItem}
        />
      </View>

      <View style={styles.footer}>
        <TouchableOpacity
          style={[
            styles.button,
            {backgroundColor: index === 0 ? '#EAEAEA' : '#F4D34F'},
          ]}
          onPress={() => {
            setIndex(index - 1);
            refItem.current.scrollToIndex({
              animated: true,
              index: '' + (index - 1),
            });
          }}
          disabled={index === 0}>
          <MArrowLeft />
        </TouchableOpacity>
        {type !== 2 && (
          <TouchableOpacity
            style={styles.submitButton}
            onPress={() => submit(tmpQuestion)}>
            <MyText style={styles.submitText}>Submit Quiz</MyText>
          </TouchableOpacity>
        )}
        <TouchableOpacity
          style={[
            styles.button,
            {
              backgroundColor:
                index === question.length - 1 ? '#EAEAEA' : '#F4D34F',
            },
          ]}
          onPress={() => {
            setIndex(index + 1);
            const nextIndex = index === question.length ? 0 : index + 1;
            refItem.current.scrollToIndex({
              animated: true,
              index: '' + nextIndex,
            });
          }}
          disabled={index === question.length - 1}>
          <MArrowRight />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingRight: Platform.OS === 'android' && 24,
    marginLeft: Platform.OS === 'ios' ? 24 : 5,
    flex: 1,
  },
  questionNum: {
    fontSize: 14,
    fontWeight: Platform.OS === 'ios' ? '500' : 'bold',
    color: '#858494',
    marginBottom: 8,
  },
  question: {
    fontSize: 20,
    marginBottom: 24,
    fontWeight: Platform.OS === 'ios' ? '500' : 'bold',
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    bottom: 50,
    position: 'absolute',
    alignSelf: 'center',
    width: '100%',
  },
  answer: {
    height: 56,
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 17,
    borderRadius: 20,
    marginBottom: 16,
  },
  option: {},
  checkBox: {
    width: 25,
    height: 25,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 11,
  },
  button: {
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
  },
  submitButton: {
    height: 50,
    width: '55%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 16,
    borderWidth: 2,
    borderColor: '#3A8C38',
  },
  submitText: {
    fontWeight: Platform.OS === 'ios' ? '700' : 'bold',
    fontSize: 18,
    color: '#3A8C38',
  },
});

export default Test;
