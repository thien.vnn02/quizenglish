import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  ImageBackground, Platform,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import {LeftArrow, Documents, Clock} from '@svg';
import {MyText} from '@components';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {selectedTest} from '../../Store/Test';
import data from '../../Resources/questions.json'
import {setType} from '../../Store/TypeOfScreen';

const LIST_QUESTIONS = [
  {
    title: 'Beginner',
    totalQuestion: 15,
    time: 10,
    image: require('../../../assets/beginer.png'),
  },
  {
    title: 'Medium',
    totalQuestion: 20,
    time: 20,
    image: require('../../../assets/medium.png'),
  },
  {
    title: 'Advance',
    totalQuestion: 30,
    time: 30,
    image: require('../../../assets/advance.png'),
  },
];

const HomeScreen = () => {
  const {navigate} = useNavigation();
  const [begin, setBegin] = useState([]);
  const [medium, setMedium] = useState([]);
  const [advance, setAdvance] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    convertData();
  }, []);

  const convertData = async () => {
    let tmpBegin = [];
    let tmpMedium = [];
    let tmpAdvance = [];
    while (data.length) {
      tmpBegin.push(data.splice(0, 15));
      tmpMedium.push(data.splice(0, 20));
      tmpAdvance.push(data.splice(0, 30));
    }

    setBegin(tmpBegin);
    setMedium(tmpMedium);
    setAdvance(tmpAdvance);
  };

  const pickerTest = (test, questions) => {
    dispatch(selectedTest({testSelected: test, questions: questions}));
  };

  const Header = () => {
    return (
      <View style={styles.header}>
        <ImageBackground
          source={require('../../../assets/backgroundDetail.png')}
          resizeMode={'cover'}
          style={{width: '100%', height: '100%'}}>
          <SafeAreaView>
            <Image
              source={require('../../../assets/name.png')}
              resizeMode={'contain'}
              style={{marginLeft: 22, marginTop: 27}}
            />
          </SafeAreaView>
        </ImageBackground>
      </View>
    );
  };

  const ItemTest = ({item, index}) => {
    const {title, totalQuestion, time, image} = item;
    return (
      <TouchableOpacity
        style={styles.containerItem}
        onPress={() => {
          navigate('Detail');
          let ques;
          switch (index) {
            case 0:
              ques = begin[Math.floor(Math.random() * begin.length)];
              break;
            case 1:
              ques = medium[Math.floor(Math.random() * medium.length)];
              break;
            case 2:
              ques = advance[Math.floor(Math.random() * advance.length)];
              break;
            default:
              return null;
          }
          dispatch(setType({type: 0}));
          pickerTest(item, ques);
        }}>
        <View style={{flexDirection: 'row'}}>
          <Image
            source={image}
            style={{width: 112, height: 84}}
            resizeMode={'contain'}
          />
          <View>
            <MyText style={{fontWeight: 'bold'}}>{title}</MyText>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Documents />
              <MyText style={{fontWeight: '100', fontSize: 14, marginLeft: 10}}>
                {totalQuestion} Questions
              </MyText>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Clock />
              <MyText style={{fontWeight: '100', fontSize: 14, marginLeft: 10}}>
                {time} min
              </MyText>
            </View>
          </View>
        </View>

        <LeftArrow />
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.content}>
        <FlatList
          data={LIST_QUESTIONS}
          renderItem={({item, index}) => <ItemTest item={item} index={index} />}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    width: '100%',
    height: '50%',
  },
  content: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: '85%',
    backgroundColor: 'white',
    borderTopStartRadius: 32,
    borderTopEndRadius: 32,
  },
  containerItem: {
    flexDirection: 'row',
    marginTop: 18,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingRight: 24,
  },
  title: {},
});

export default HomeScreen;
