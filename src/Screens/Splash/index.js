import React, {useEffect, useState} from 'react';
import {StyleSheet, View, Image, ImageBackground} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const SplashScreen = () => {
  const {navigate} = useNavigation();

  useEffect(() => {
    navigate('Home');
  }, []);

  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../../../assets/background.png')}
        style={styles.background}>
        <Image
          source={require('../../../assets/logo.png')}
          resizeMode={'contain'}
          style={{width: 124, height: 124, marginBottom: 34}}
        />
        <Image source={require('../../../assets/name.png')} />
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SplashScreen;
