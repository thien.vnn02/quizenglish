import React from 'react';
import { Platform, StyleSheet, View } from "react-native";
import {MyText} from '@components';
import AnimateNumber from 'react-native-animate-number';

const Result = ({percent, total, correct, wrong}) => {
  const Info = ({number, description, color}) => {
    return (
      <View style={{marginBottom: 25}}>
        <MyText style={[styles.number, {color: color}]}>
          ●{' '}
          {
            <AnimateNumber
              value={number}
              countBy={1}
              style={{fontFamily: 'Rubik'}}
              onProgress={() => {}}
            />
          }{' '}
          {description === 'Completation' && '%'}
        </MyText>
        <MyText style={styles.description}>{description}</MyText>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <View>
        <Info description="Completation" number={parseFloat(percent.toFixed(2))} color="#3DA2FF" />
        <Info description="Correct" number={correct} color="#3A8C38" />
      </View>
      <View>
        <Info description="Total Question" number={total} color="#F6C15D" />
        <Info description="Wrong" number={wrong} color="#CA0E0E" />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 20,
    borderWidth: 2,
    borderColor: '#DDDDDD',
    marginRight: 24,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 34,
    paddingRight: 23,
    paddingTop: 23,
  },
  number: {
    fontWeight: Platform.OS === 'ios' ? '900': 'bold',
  },
  description: {
    marginLeft: 13,
    fontSize: 14,
    fontWeight: Platform.OS === 'ios' ? '700' : 'bold',
  },
});
export default Result;
