import React, {useState} from 'react';
import {
  ImageBackground,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {MyText} from '@components';
import Result from './components/Result';
import {Home, Chart, RollBack} from '@svg';
import {useNavigation, useRoute} from '@react-navigation/native';
import AnimateNumber from 'react-native-animate-number';
import {useDispatch, useSelector} from 'react-redux';
import {setType} from '../../Store/TypeOfScreen';

const ResultScreen = () => {
  const {navigate} = useNavigation();
  const {params} = useRoute();
  const dispatch = useDispatch();
  const result = useSelector(e => e.result);
  const {correct, percent, score, total, wrong} = result;

  const Header = () => {
    return (
      <View style={styles.header}>
        <ImageBackground
          source={require('../../../assets/backgroundResult.png')}
          resizeMode={'cover'}
          style={styles.backgroundHeader}>
          <ImageBackground
            source={require('../../../assets/bgScore.png')}
            style={styles.backgroundScore}
            resizeMode={'contain'}>
            <View style={styles.contentHeader}>
              <MyText style={styles.textHeader}>YOUR SCORE</MyText>
              <View style={{flexDirection: 'row', justifyContent: 'flex-end'}}>
                <AnimateNumber
                  value={parseFloat(score.toFixed(1))}
                  countBy={1}
                  style={{
                    fontSize: 50,
                    fontWeight: Platform.OS === 'ios' ? '600' : 'bold',
                    fontFamily: 'Rubik',
                  }}
                  onProgress={() => {}}
                />
                <MyText style={styles.score}>pt</MyText>
              </View>
            </View>
          </ImageBackground>
        </ImageBackground>
      </View>
    );
  };

  const Button = ({icon, background, name, onPress}) => {
    return (
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <TouchableOpacity
          style={[styles.button, {backgroundColor: background}]}
          onPress={onPress}>
          {icon}
        </TouchableOpacity>
        <MyText style={styles.nameButton}>{name}</MyText>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.content}>
        <Result
          correct={correct}
          total={total}
          percent={percent}
          wrong={wrong}
        />
        <View style={styles.footer}>
          <Button
            icon={<Home />}
            background="#1BC0E4"
            name="Home"
            onPress={() => navigate('Home')}
          />
          <Button
            icon={<RollBack />}
            background="#3DA2FF"
            name="Text Again"
            onPress={() => {
              navigate('Detail');
              dispatch(setType({type: 1}));
              params.restart();
            }}
          />
          <Button
            icon={<Chart />}
            background="#F6AD48"
            name="Review"
            onPress={() => {
              navigate('Detail');
              dispatch(setType({type: 2}));
              params.restart();
            }}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  header: {
    width: '100%',
    height: '50%',
  },
  backgroundHeader: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  contentHeader: {
    alignSelf: 'center',
    justifyContent: 'center',
  },
  textHeader: {
    fontSize: 18,
    fontWeight: Platform.OS === 'ios' ? '500' : 'bold',
    alignSelf: 'center',
  },
  score: {
    alignSelf: 'center',
    fontSize: 24,
    fontWeight: Platform.OS === 'ios' ? '600' : 'bold',
    lineHeight: 59,
  },
  backgroundScore: {
    width: 247,
    height: 247,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    position: 'absolute',
    width: '100%',
    height: '55%',
    bottom: 0,
    backgroundColor: 'white',
    borderTopEndRadius: 32,
    borderTopStartRadius: 32,
    paddingTop: 26,
    paddingLeft: 24,
  },
  button: {
    width: 56,
    height: 56,
    borderRadius: 28,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 30,
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: 50,
    marginTop: 119,
  },
  nameButton: {
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    marginLeft: 40,
    marginTop: 10,
    fontWeight: Platform.OS === 'ios' ? '500' : 'bold',
  },
});

export default ResultScreen;
