import {createSlice} from '@reduxjs/toolkit';

const initState = {
  score: 0,
  percent: 0,
  total: 0,
  correct: 0,
  wrong: 0,
  answer: [],
};

export const resultSlice = createSlice({
  name: 'result',
  initialState: initState,
  reducers: {
    computeResult: (state, action) => {
      const {question} = action.payload;
      const completeQuestion = question.filter(
        e =>
          e.A.isSelected || e.B.isSelected || e.C.isSelected || e.D.isSelected,
      );
      state.percent = (completeQuestion.length / question.length) * 100;
      const correct = question.filter(
        e =>
          (e.A.isSelected && e.A.isCorrect) ||
          (e.B.isSelected && e.B.isCorrect) ||
          (e.C.isSelected && e.C.isCorrect) ||
          (e.D.isSelected && e.D.isCorrect),
      );
      state.correct = correct.length;
      state.wrong = question.length - correct.length;
      state.total = question.length;
      state.score = (100/question.length) * correct.length;
      state.answer = question;
    },
  },
});

export const {computeResult} = resultSlice.actions;
export default resultSlice.reducer;
