import {createSlice} from '@reduxjs/toolkit';

const initState = {
  type: 0,
};

export const answerSlice = createSlice({
  name: 'type',
  initialState: initState,
  reducers: {
    setType: (state, action) => {
      state.type = action.payload.type;
    },
  },
});

export const {setType} = answerSlice.actions;
export default answerSlice.reducer;
