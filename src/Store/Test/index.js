import {createSlice} from '@reduxjs/toolkit';

const initState = {
  testSelect: {},
  questions: [],
};

export const testSlice = createSlice({
  name: 'test',
  initialState: initState,
  reducers: {
    selectedTest: (state, action) => {
      state.testSelect = action.payload.testSelected;
      state.questions = action.payload.questions;
    },
  },
});

export const {selectedTest} = testSlice.actions;
export default testSlice.reducer;
