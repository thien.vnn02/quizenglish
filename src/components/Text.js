import React from 'react';
import {Text} from 'react-native';

export const textStyles = {
  fontFamily: 'Rubik',
  fontWeight: 'normal',
  fontSize: 16,
  lineHeight: 22.4,
  color: 'black',
};

export const MyText = ({style, ...props}) => (
  <Text {...props} style={[textStyles, style]} />
);
